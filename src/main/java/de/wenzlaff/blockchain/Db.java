package de.wenzlaff.blockchain;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.store.storage.embedded.types.EmbeddedStorage;
import org.eclipse.store.storage.embedded.types.EmbeddedStorageManager;
import org.eclipse.store.storage.restservice.types.StorageRestService;
import org.eclipse.store.storage.restservice.types.StorageRestServiceResolver;

import de.wenzlaff.blockchain.be.Adresse;

/**
 * https://github.com/eclipse-store/store/tree/main
 * https://docs.eclipsestore.io/manual/storage/getting-started.html
 * 
 * http://localhost:4567/ // http://localhost:4567/store-data/root //
 * http://localhost:4567/store-data/object/1000000000000000071
 * 
 * http://localhost:4567/store-data
 * 
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Db {

	private static final Logger LOG = LogManager.getLogger(Db.class);

	public static void main(String[] args) {

		EmbeddedStorageManager storage = EmbeddedStorage.start();

		Adresse buyMeACoffeeBtc = new Adresse();
		buyMeACoffeeBtc.setAdresse("bc1qj4grttyhk2h5wqask3nku70e3qtycssz5kvw5l");
		if (buyMeACoffeeBtc.isValid()) {

			if (storage.root() == null) {
				LOG.info("Keine DB vorhanden, anlegen in ../storage Verzeichnis ...");
				storage.setRoot(buyMeACoffeeBtc);
				storage.storeRoot();
			} else {
				LOG.info("DB vorhanden, verwenden ...");
				Adresse gespeicherteRootAdresse = (Adresse) storage.root();
				LOG.info("Gespeicherte Root Adresse: " + gespeicherteRootAdresse);
			}
			// und eine REST-Service starten ... http://localhost:4567/store-data/root
			StorageRestService service = StorageRestServiceResolver.resolve(storage);
			service.start();
		}
		// storageManager.shutdown();
	}
}
