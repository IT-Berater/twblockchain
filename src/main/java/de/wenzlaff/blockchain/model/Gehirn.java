package de.wenzlaff.blockchain.model;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.RemoteFunctionCall;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 4.8.7.
 */
@SuppressWarnings("rawtypes")
public class Gehirn extends Contract {
    public static final String BINARY = "608060405234801561001057600080fd5b5060646000556103d3806100256000396000f3fe6080604052600436106100345760003560e01c806303da5f75146100395780634035548114610060578063cfb996da1461007f575b600080fd5b34801561004557600080fd5b5061004e610109565b60408051918252519081900360200190f35b61007d6004803603602081101561007657600080fd5b5035610110565b005b34801561008b57600080fd5b50610094610115565b6040805160208082528351818301528351919283929083019185019080838360005b838110156100ce5781810151838201526020016100b6565b50505050905090810190601f1680156100fb5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b6000545b90565b600055565b6060602860005411610150575060408051808201909152601281527112d95a5b9948105d5cdcd859d95adc98599d60721b602082015261010d565b6029600054101580156101665750604660005411155b1561018b5760405180606001604052806033815260200161036b60339139905061010d565b6047600054101580156101a15750604f60005411155b156101d857506040805180820190915260158152740eadce8cae4c8eae4c6d0e6c6d0dcd2e8e8d8d2c6d605b1b602082015261010d565b6050600054101580156101ee5750605960005411155b1561022d575060408051808201909152601b81527f657477617320756e74657264757263687363686e6974746c6963680000000000602082015261010d565b605a600054101580156102435750606d60005411155b15610271575060408051808201909152600c81526b111d5c98da1cd8da1b9a5d1d60a21b602082015261010d565b606e600054101580156102875750607760005411155b156102ad57506040805180820190915260048152630d0dec6d60e31b602082015261010d565b6078600054101580156102c35750608160005411155b156102ee57506040805180820190915260098152680e6cad0e440d0dec6d60bb1b602082015261010d565b6082600054101580156103045750609f60005411155b15610330575060408051808201909152600a8152691a1bd8da189959d8589d60b21b602082015261010d565b60a0600054111561010d575060408051808201909152601281527112d95a5b9948105d5cdcd859d95adc98599d60721b602082015261010d56fe5765697420756e74657264757263687363686e6974746c69636820e2809320476569737469676520426568696e646572756e67a2646970667358221220e47f5ca58cc85c126b32581ede151a0854e01cddae7e74c48381e24fa57c5ed964736f6c634300060c0033";

    public static final String FUNC_GETBEDEUTUNG = "getBedeutung";

    public static final String FUNC_GETIQ = "getIQ";

    public static final String FUNC_SETIQ = "setIQ";

    @Deprecated
    protected Gehirn(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected Gehirn(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected Gehirn(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected Gehirn(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public RemoteFunctionCall<String> getBedeutung() {
        final Function function = new Function(FUNC_GETBEDEUTUNG, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<BigInteger> getIQ() {
        final Function function = new Function(FUNC_GETIQ, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<TransactionReceipt> setIQ(BigInteger neuerIQ) {
        final Function function = new Function(
                FUNC_SETIQ, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(neuerIQ)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    @Deprecated
    public static Gehirn load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Gehirn(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static Gehirn load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Gehirn(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static Gehirn load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new Gehirn(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static Gehirn load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new Gehirn(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<Gehirn> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(Gehirn.class, web3j, credentials, contractGasProvider, BINARY, "");
    }

    public static RemoteCall<Gehirn> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(Gehirn.class, web3j, transactionManager, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<Gehirn> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Gehirn.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<Gehirn> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Gehirn.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }
}
