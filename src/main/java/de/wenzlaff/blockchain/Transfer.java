package de.wenzlaff.blockchain;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.ConnectException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGasPrice;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.utils.Convert;
import org.web3j.utils.Convert.Unit;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * Klasse zum abfragen des Bestands einer Adresse auf der ETH Blockchain und
 * versenden von Eth von einem File Wallet.
 * 
 * https://docs.web3j.io/4.8.7/
 * 
 * @author Thomas Wenzlaff
 */
@Command(name = "Transfer", mixinStandardHelpOptions = true, version = "Transfer 1.0", description = "Eine transaktion ausführen.", footer = { "@|fg(green) Thomas Wenzlaff|@",
		"@|fg(red),bold http://www.wenzlaff.info|@" })
public class Transfer implements Callable<Integer> {

	private static final Logger LOG = LogManager.getLogger(Transfer.class);

	@Option(names = { "-s", "--server" }, description = "die URL des RPC (Ganache) Servers", defaultValue = "http://127.0.0.1:7545")
	private static String rpcServerUrl;

	@Option(names = { "-p", "--password" }, description = "das Passwort aus der Quell-Wallet")
	private static String walletPasswordQuelle;

	@Option(names = { "-d", "--walletDateiname" }, description = "der Dateiname des Json Wallet der Quelle")
	private static String walletJsonDateinameQuelle; // z.B.
														// "UTC--2021-09-25T15-09-47.203000000Z--8e031f1c47537910fe104936dcd0f77f8a97c6f1.json"
	@Option(names = { "-z", "--zielAdresse" }, description = "die Ziel Adresse")
	private static String zielAdresseJson; // z.B. 0x1486df323a85999fb6a540492dc8725c8ecdfb9b
	// Ganache Address z.B. die 1. aus der Liste

	@Option(names = { "-b", "--betrag" }, description = "derBetrag der überwiesen werden soll in Ether")
	private static double ueberweiseBetragInEther; // z.B. 0.02

	/**
	 * Parameter: Passwort JsonFileName Ziel-Adresse z.B.:
	 * 
	 * <pre>
		-p password 
		-d UTC--2021-09-25T15-09-47.203000000Z--8e031f1c47537910fe104936dcd0f77f8a97c6f1.json 
		-z 0xDaAaf72E5E18F040116dA4495EF7f5c1376E8C8a
		-b 0.015
	 * </pre>
	 * 
	 * @param args siehe Beschreibung
	 * @throws Exception bei Fehler
	 */
	public static void main(String[] args) throws Exception {

		int exitCode = new CommandLine(new Transfer()).execute(args);
		System.exit(exitCode);
	}

	private void send(Web3j web3, Credentials credentials, String zielAdresse, double ether) throws Exception {
		LOG.info("Send " + ether + " Ether  = " + Convert.toWei(BigDecimal.valueOf(ether), Unit.ETHER) + " WEI ...");
		TransactionReceipt transactionReceipt = org.web3j.tx.Transfer.sendFunds(web3, credentials, zielAdresse, BigDecimal.valueOf(ether), Convert.Unit.ETHER).send();
		LOG.info("Betrag von Adresse: " + transactionReceipt.getFrom());
		LOG.info("In Block Nummer   : " + transactionReceipt.getBlockNumber());
		request(web3, zielAdresse);
	}

	private Web3j getInstance(String blockchainServerUrl) {
		Web3j web3 = Web3j.build(new HttpService(rpcServerUrl));
		LOG.info("OK, verbunden mit Blockchain " + blockchainServerUrl + " ...");
		return web3;
	}

	private void request(Web3j web3, String addresse) throws IOException, InterruptedException, ExecutionException {

		Web3ClientVersion clientVersion = null;
		try {
			clientVersion = web3.web3ClientVersion().send();
		} catch (ConnectException e) {
			LOG.error("Die Blockchain ist nicht erreichbar. Starten vergessen? Oder URL oder Port falsch?");
			throw e;

		}
		LOG.debug("Client Version: " + clientVersion.getWeb3ClientVersion());

		EthGasPrice gasPrice = web3.ethGasPrice().send();

		LOG.debug("Default Gas Preis: " + gasPrice.getGasPrice());
		LOG.info("Addresse:           " + addresse);
		LOG.info("Bestand:            " + Convert.fromWei(web3.ethGetBalance(addresse, DefaultBlockParameterName.LATEST).send().getBalance().toString(), Unit.ETHER) + " Ether =  "
				+ Convert.fromWei(web3.ethGetBalance(addresse, DefaultBlockParameterName.LATEST).send().getBalance().toString(), Unit.GWEI) + " GWEI");
	}

	@Override
	public Integer call() throws Exception {

		LOG.info("Kopiere " + ueberweiseBetragInEther + " Ether von Json Wallet Datei --> " + walletJsonDateinameQuelle);
		LOG.info("nach Ziel Konto Adresse --> " + zielAdresseJson);

		Transfer transfer = new Transfer();
		Web3j web3 = transfer.getInstance(rpcServerUrl);
		LOG.info("Ziel Addresse Bestand: ");
		transfer.request(web3, zielAdresseJson);

		Credentials jsonWalletCredentials = WalletUtils.loadCredentials(walletPasswordQuelle, walletJsonDateinameQuelle);

		LOG.info("Von Json Wallet Adresse Bestand: ");
		transfer.request(web3, jsonWalletCredentials.getAddress());

		transfer.send(web3, jsonWalletCredentials, zielAdresseJson, ueberweiseBetragInEther);

		return 0;
	}
}
