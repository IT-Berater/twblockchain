package de.wenzlaff.blockchain;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.Provider;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Zertifikate checken.
 * 
 * @author Thomas Wenzlaff
 */
public class Zertifikat {

	private static final Logger LOG = LogManager.getLogger(Zertifikat.class);

	public static void main(String[] args) throws Exception {

		// printSecurityProviders();

		// https://docs.oracle.com/en/java/javase/11/docs/specs/security/standard-names.html#certificatefactory-types
		// https://www.baeldung.com/java-bouncy-castle

		// http://blog.wenzlaff.de/?p=17283
		// ca
		// http://blog.wenzlaff.de/?p=17337
		// Java
		// http://blog.wenzlaff.de/?p=16427

		checkValidityCertificate("target/classes/ecc/junitec.crt");
	}

	public static boolean checkValidityCertificate(String certificate) throws CertificateException, FileNotFoundException {
		boolean isValid = false;
		try {
			CertificateFactory fac = CertificateFactory.getInstance("X509");
			FileInputStream is = new FileInputStream(certificate);
			X509Certificate cert = (X509Certificate) fac.generateCertificate(is);

			cert.checkValidity(); // Checks that the certificate is currently valid It is if the current date and
									// time are within the validity period given in the certificate
			isValid = true;

			LOG.info("Certificate Type: " + cert.getType());
			LOG.info("Signature algorithm name: " + cert.getSigAlgName());
			LOG.info("Issuer distinguished name (IssuerDN): " + cert.getIssuerX500Principal());
			LOG.info("SerialNumber (the issuer name and serial number identify a unique certificate): " + cert.getSerialNumber());
			LOG.info("From : " + cert.getNotBefore());
			LOG.info("Until: " + cert.getNotAfter());
		} catch (CertificateExpiredException e) {
			LOG.error("Zertifikat abgelaufen. " + e.getLocalizedMessage());
			isValid = false;
		} catch (CertificateNotYetValidException e) {
			LOG.error("Zertifikat ungültig. " + e.getLocalizedMessage());
			isValid = false;
		} catch (CertificateException e) {
			LOG.error("Zertifikat ausnahme. " + e.getLocalizedMessage());
			isValid = false;
		}
		LOG.info("Certificate isValid: " + isValid);
		return isValid;
	}

	public static void printSecurityProviders() {
		Provider[] providers = Security.getProviders();

		for (Provider provider : providers) {
			LOG.info("Provider: " + provider);
		}
	}
}
