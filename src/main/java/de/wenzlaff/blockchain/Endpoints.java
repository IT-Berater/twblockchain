package de.wenzlaff.blockchain;

/**
 * Mögliche Endpoints von https://infura.io/dashboard/ethereum/
 * 
 * @author Thomas Wenzlaff
 */
public enum Endpoints {
	mainnet("mainnet"), ropsten("ropsten"), kovan("kovan"), rinkeby("rinkeby"), görli("goerli"), polygon_mainnet("polygon-mainnet"), polygon("polygon"),
	polygon_mumbai("polygon-mumbai"), arbitrum_mainnet("arbitrum-mainnet"), arbitrum_rinkeby("arbitrum-rinkeby"), optimism_mainnet("optimism-mainnet"),
	optimism_kovan("optimism-kovan");

	String urlName;

	Endpoints(String name) {
		urlName = name;
	}
}