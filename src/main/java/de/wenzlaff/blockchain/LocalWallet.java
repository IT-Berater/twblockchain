package de.wenzlaff.blockchain;

import java.io.File;
import java.io.FileReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;

/**
 * Erzeugt ein Wallet in einem Json File.
 * 
 * Aufruf: LocalWallet [Passwort]
 * 
 * z.B.
 * 
 * LocalWallet geheim
 * 
 * Erzeugt z.B. solch ein Json File:
 * 
 * <pre>
 {
	"address": "1486df323a85999fb6a540492dc8725c8ecdfb9b",
	"id": "12a615ec-1926-4a0c-b6c9-355d525c3fd1",
	"version": 3,
	"crypto": {
		"cipher": "aes-128-ctr",
		"ciphertext": "a367817d4cb23e822d4f46ec726dd7717f20f3e19a1b651382bc7b156a2bf306",
		"cipherparams": {
			"iv": "f60a43fa2d8a0fc988e430f4f63d70e8"
		},
		"kdf": "scrypt",
		"kdfparams": {
			"dklen": 32,
			"n": 262144,
			"p": 1,
			"r": 8,
			"salt": "4c9e3b621a9df62fb5e1aaa9323ca6c508908976af0adb4f5680685c13d1bde9"
		},
		"mac": "248f3ccfcca62b810ef63daef1bec2633c96c657712f75f056e0c9c9e379083a"
	}
}
 * </pre>
 * 
 * Auf die Adresse des erzeugten Wallets aus dem File z.B. "address":
 * "8e031f1c47537910fe104936dcd0f77f8a97c6f1", per MetaMask Guthaben überweisen
 * 
 * @author Thomas Wenzlaff
 */
public class LocalWallet {

	private static final Logger LOG = LogManager.getLogger(LocalWallet.class);

	/**
	 * Startet das generieren eines File Wallets
	 * 
	 * @param args Passwort
	 * @throws Exception bei Fehlern
	 */
	public static void main(String[] args) throws Exception {

		if (args.length == 0 || args[0].isEmpty()) {
			LOG.error("Passwort nicht als Programmparameter gesetzt!");
			return;
		}
		String password = args[0];

		LocalWallet localWallet = new LocalWallet();
		String jsonWalletFileName = localWallet.createWallet(password);
		LOG.info("Erzeugte Wallet Adresse: " + localWallet.readJsonWallet(jsonWalletFileName).get("address"));

		Credentials credentials = WalletUtils.loadCredentials(password, jsonWalletFileName);
		LOG.info("Credential: " + credentials.getAddress());
		LOG.info("Private Key: " + credentials.getEcKeyPair().getPrivateKey());
		LOG.info("Public Key : " + credentials.getEcKeyPair().getPublicKey());
	}

	/**
	 * Erzeugt das Json Wallet.
	 * 
	 * @param password das Passwort
	 * @throws Exception bei Fehlern
	 */
	private String createWallet(String password) throws Exception {

		String generateWalletFileName = WalletUtils.generateNewWalletFile(password, new File("."));
		LOG.info("Wallet mit Passwort: " + password + " in Json Datei: " + generateWalletFileName + " erzeugt.");
		return generateWalletFileName;
	}

	/**
	 * Liest eine Json Wallet Datei und gib sie als Json Objekt zurück.
	 * 
	 * @param walletDateiName der Dateiname
	 * @return das File Wallet als Json
	 */
	private JSONObject readJsonWallet(String walletDateiName) {

		JSONObject wallet = null;

		try (FileReader reader = new FileReader(walletDateiName)) {
			Object obj = new JSONParser().parse(reader);
			wallet = (JSONObject) obj;
		} catch (Exception e) {
			LOG.error("Fehler beim einlesen des Wallets", e);
		}
		return wallet;
	}
}