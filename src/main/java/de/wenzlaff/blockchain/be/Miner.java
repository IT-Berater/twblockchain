package de.wenzlaff.blockchain.be;

/**
 * Ein Miner, der die Transaktionen vergleicht und sie gegeben Gebühr (fee) dem
 * Block hinzufügt.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Miner {

	private Fee fee;

	public Miner() {

	}

	public Fee getFee() {
		return fee;
	}

	public void setFee(Fee fee) {
		this.fee = fee;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fee == null) ? 0 : fee.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Miner other = (Miner) obj;
		if (fee == null) {
			if (other.fee != null)
				return false;
		} else if (!fee.equals(other.fee))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Miner [");
		if (fee != null) {
			builder.append("fee=");
			builder.append(fee);
		}
		builder.append("]");
		return builder.toString();
	}

}
