package de.wenzlaff.blockchain.be;

/**
 * Die Gebühr.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Fee {

	private Double betrag;

	public Fee() {

	}

	public Double getBetrag() {
		return betrag;
	}

	public void setBetrag(Double betrag) {
		this.betrag = betrag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((betrag == null) ? 0 : betrag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fee other = (Fee) obj;
		if (betrag == null) {
			if (other.betrag != null)
				return false;
		} else if (!betrag.equals(other.betrag))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Fee [");
		if (betrag != null) {
			builder.append("betrag=");
			builder.append(betrag);
		}
		builder.append("]");
		return builder.toString();
	}

}
