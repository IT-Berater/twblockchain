package de.wenzlaff.blockchain.be;

/**
 * Eine Transaktion, die von Minern bei gültigkeit dem Block und damit der
 * Blockchain hinzugefügt werden.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Transaktion {

	private Fee fee;

	private Double betrag;

	private Adresse ziel;

	private Adresse von;

	public Transaktion() {

	}

	public boolean isValid() {
		return true;
	}

	public Fee getFee() {
		return fee;
	}

	public void setFee(Fee fee) {
		this.fee = fee;
	}

	public Double getBetrag() {
		return betrag;
	}

	public void setBetrag(Double betrag) {
		this.betrag = betrag;
	}

	public Adresse getZiel() {
		return ziel;
	}

	public void setZiel(Adresse ziel) {
		this.ziel = ziel;
	}

	public Adresse getVon() {
		return von;
	}

	public void setVon(Adresse von) {
		this.von = von;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((betrag == null) ? 0 : betrag.hashCode());
		result = prime * result + ((fee == null) ? 0 : fee.hashCode());
		result = prime * result + ((von == null) ? 0 : von.hashCode());
		result = prime * result + ((ziel == null) ? 0 : ziel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaktion other = (Transaktion) obj;
		if (betrag == null) {
			if (other.betrag != null)
				return false;
		} else if (!betrag.equals(other.betrag))
			return false;
		if (fee == null) {
			if (other.fee != null)
				return false;
		} else if (!fee.equals(other.fee))
			return false;
		if (von == null) {
			if (other.von != null)
				return false;
		} else if (!von.equals(other.von))
			return false;
		if (ziel == null) {
			if (other.ziel != null)
				return false;
		} else if (!ziel.equals(other.ziel))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Transaktion [");
		if (fee != null) {
			builder.append("fee=");
			builder.append(fee);
			builder.append(", ");
		}
		if (betrag != null) {
			builder.append("betrag=");
			builder.append(betrag);
			builder.append(", ");
		}
		if (ziel != null) {
			builder.append("ziel=");
			builder.append(ziel);
			builder.append(", ");
		}
		if (von != null) {
			builder.append("von=");
			builder.append(von);
		}
		builder.append("]");
		return builder.toString();
	}

}
