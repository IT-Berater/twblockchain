package de.wenzlaff.blockchain.be;

import java.util.List;

/**
 * Ein Mempool.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Mempool {

	private List<Transaktion> transaktionen;

	public Mempool() {

	}

	public List<Transaktion> getTransaktionen() {
		return transaktionen;
	}

	public void setTransaktionen(List<Transaktion> transaktionen) {
		this.transaktionen = transaktionen;
	}

	/**
	 * TODO
	 * 
	 * @param transaktion die Transakion
	 */
	public void addTransaktin(Transaktion transaktion) {

		transaktionen.add(transaktion);
	}

	/**
	 * TODO
	 * 
	 * @return alle gültigen Transaktionen
	 */
	public List<Transaktion> getAlleGültigenTransaktionen() {
		return transaktionen;
	}
}
