package de.wenzlaff.blockchain.be;

/**
 * Eine BTC Adresse.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Adresse {

	private String adresse;

	public Adresse() {

	}

	public boolean isValid() {
		return true;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adresse == null) ? 0 : adresse.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Adresse other = (Adresse) obj;
		if (adresse == null) {
			if (other.adresse != null)
				return false;
		} else if (!adresse.equals(other.adresse))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Adresse [");
		if (adresse != null) {
			builder.append("adresse=");
			builder.append(adresse);
		}
		builder.append("]");
		return builder.toString();
	}

}
