package de.wenzlaff.blockchain.be;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Der Block einer Blockchain.
 * 
 * Ein einfaches POJO. Es können alle Daten enthalten sein. Hier mal als String.
 * 
 * Der Hash des Blocks berechnet sich aus den enthaltenen Daten (Transaktionen),
 * dem Hash des vorherigen Blocks, der aktuellen Uhrzeit und einer zufälligen
 * Zahl (Nonce).
 * 
 * @author Thomas Wenzlaff
 */
public class Block {

	private static final Logger LOG = LogManager.getLogger(Block.class);

	/**
	 * Der Hash vom vorherigen Block.
	 */
	private int previousHash;
	/**
	 * Die Daten. Es könnte alles sein (Transaktionen).
	 */
	private List<Transaktion> data;

	/**
	 * Der Hash dieses Blockes.
	 */
	private int hash;

	public Block(List<Transaktion> data, int previousHash) {
		this.data = data;
		this.previousHash = previousHash;

		Integer uhrzeit = LocalDateTime.now().toLocalTime().toSecondOfDay();
		Integer zufall = new Random().nextInt();
		// eine Verbindung der gehashten Daten (mit Arrays.hashCode) mit den
		// gehashten
		// Daten aus dem vorhergehenden Block also das macht die Blockchain aus
		// und Uhrzeit und Zufallszahl
		this.hash = Arrays.hashCode(new Integer[] { data.hashCode(),
				previousHash, uhrzeit, zufall });
		LOG.info("Neuen Block erzeugt mit Hash: " + this.hash);
	}

	public boolean isValid() {
		return true;
	}

	/**
	 * TODO: Mining
	 * 
	 * @return der Block
	 */
	public Block mining() {
		Fee miningFee = new Fee();
		miningFee.setBetrag(6.25); // BTC für einen Block
		Block block = null;

		return null;
	}

	public int getPreviousHash() {
		return previousHash;
	}

	public void setPreviousHash(int previousHash) {
		this.previousHash = previousHash;
	}

	public List<Transaktion> getData() {
		return data;
	}

	public void setData(List<Transaktion> data) {
		this.data = data;
	}

	public int getHash() {
		return hash;
	}

	public void setHash(int hash) {
		this.hash = hash;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + hash;
		result = prime * result + previousHash;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Block other = (Block) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (hash != other.hash)
			return false;
		if (previousHash != other.previousHash)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Block [previousHash=");
		builder.append(previousHash);
		builder.append(", ");
		builder.append("hash=");
		builder.append(hash);
		if (data != null) {
			builder.append(", ");
			builder.append("data=");
			builder.append(data);
		}
		builder.append("]");
		return builder.toString();
	}
}
