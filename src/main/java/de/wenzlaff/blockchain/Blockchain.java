package de.wenzlaff.blockchain;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.wenzlaff.blockchain.be.Block;

/**
 * Blockchain.
 * 
 * @author Thomas Wenzlaff
 */
public class Blockchain {

	private static final Logger LOG = LogManager.getLogger(Blockchain.class);

	/** Die Blockchain. */
	private List<Block> chain;

	public Blockchain() {
		chain = new ArrayList<>();
	}

	public boolean add(Block block) {
		return chain.add(block);
	}

	public boolean checkBlockchain() {
		boolean ergebnis = validate();
		if (ergebnis) {
			LOG.info("Die Blockchain ist gültig.");
		} else {
			LOG.error("Die Blockchain ist gehackt worden und ungültig.");
		}
		return ergebnis;
	}

	public void printBlockchain() {
		chain.forEach(b -> LOG.info(b));
	}

	public int getPreviousHash() {
		return chain.get(getAnzahlBlocks() - 1).getHash();
	}

	public Block get(int i) {
		return chain.get(i);
	}

	public void remove(int i) {
		chain.remove(i);
	}

	public void add(int i, Block block) {
		chain.add(i, block);
	}

	public int getAnzahlBlocks() {
		return chain.size();
	}

	public boolean validate() {

		boolean result = true;

		Block lastBlock = null;
		for (int i = getAnzahlBlocks() - 1; i >= 0; i--) {
			if (lastBlock == null) {
				lastBlock = get(i);
			} else {
				Block current = get(i);
				if (lastBlock.getPreviousHash() != current.getHash()) {
					result = false;
					break;
				}
				lastBlock = current;
			}
		}
		return result;
	}
}