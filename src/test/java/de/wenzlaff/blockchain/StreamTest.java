package de.wenzlaff.blockchain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.wenzlaff.blockchain.be.Adresse;

/**
 * Stream Test BTC unt ETH.
 *
 * @author Thomas Wenzlaff
 */
public class StreamTest {

	public static void main(String[] args) {

		List<Adresse> adressen = new ArrayList<>();

		Adresse meineByMeACoffeeBTCAdresse = new Adresse();
		meineByMeACoffeeBTCAdresse
				.setAdresse("bc1qj4grttyhk2h5wqask3nku70e3qtycssz5kvw5l");

		Adresse meineETHByMeACoffeeAdresse = new Adresse();
		meineETHByMeACoffeeAdresse
				.setAdresse("0x829F9e57c29ab683E964c76160B7B0BaB2727dD2");

		Adresse nochEineDummyAdresse = new Adresse();
		nochEineDummyAdresse.setAdresse("999999999999999999");

		Adresse nochEineWeitereDummyAdresse = new Adresse();
		nochEineWeitereDummyAdresse.setAdresse("22222222222222");

		adressen.add(meineByMeACoffeeBTCAdresse);
		adressen.add(meineETHByMeACoffeeAdresse);
		adressen.add(nochEineDummyAdresse);
		adressen.add(nochEineWeitereDummyAdresse);

		// wir suchen in allen Adressen nach einer 5 und finden meine beiden BTC
		// und ETH Adressen mit stream und filter Collectors
		List<Adresse> gefilterteAdressen = adressen.stream()
				.filter(z -> z.getAdresse().contains("5"))
				.collect(Collectors.toList());

		// letzter Block 758625
		// System.out.println("Meine BTC und ETH Adresse: " +
		// gefilterteAdressen);
	}
}
