package de.wenzlaff.blockchain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import de.wenzlaff.blockchain.be.Block;
import de.wenzlaff.blockchain.be.Transaktion;

/**
 * Testet die Blockchain.
 *
 * @author Thomas Wenzlaff
 *
 */
class BlockchainTest {

	private static final Logger LOG = LogManager.getLogger(BlockchainTest.class);

	/** Die zu testende Blockchain. */
	private Blockchain chain;

	// TODO: Transaktionen in aus den Mempool in den Block durch Miner der dann
	// belohnt wird
	@BeforeEach
	void iniBlockchain() {
		chain = new Blockchain();

		List<Transaktion> testTransaktion = new ArrayList<>();
		Transaktion t = new Transaktion();
		t.isValid(); // TODO: es werden nur gültige Transaktionen hinzugefügt
		testTransaktion.add(t);

		Block genesisBlock = new Block(testTransaktion, 0); // "Erster Block (genesis) der Blockchain"
		genesisBlock.isValid(); // TODO: es werden nur gültige Blöcke der Blockchain hinzugefügt

		chain.add(genesisBlock);

		Block zweiterBlock = new Block(testTransaktion, chain.getPreviousHash()); // "Zweiter Block"
		chain.add(zweiterBlock);

		Block dritterBlock = new Block(testTransaktion, chain.getPreviousHash()); // "Dritter Block mit Daten"
		chain.add(dritterBlock);

		Block vierterBlock = new Block(testTransaktion, chain.getPreviousHash()); // "Vierter Block auch mit super Daten."
		chain.add(vierterBlock);

		Block letzterBlock = new Block(testTransaktion, chain.getPreviousHash()); // "5. und letzter Block"
		chain.add(letzterBlock);

		LOG.info("Blockchain mit " + chain.getAnzahlBlocks() + " Blöcken für Test erzeugt.");
	}

	/**
	 * Positiver Test.
	 */
	@Test
	void testBlockchain() {

		chain.printBlockchain();
		assertTrue(chain.checkBlockchain());
	}

	/**
	 * wie eine veränderte Blockchain erkannt wird: ersetzen des zweiten Block durch
	 * einen veränderten bzw. gehackten Block. Es passen die Hash nicht mehr
	 */
	@Test
	void testBlockchainHack() {

		List<Transaktion> testTransaktion = new ArrayList<>();
		Transaktion t = new Transaktion();
		testTransaktion.add(t);
		Block gehackterBlock = new Block(testTransaktion, chain.get(0).getHash()); // "Verfälschte Daten"
		chain.remove(1);
		chain.add(1, gehackterBlock);

		chain.printBlockchain();
		assertFalse(chain.checkBlockchain());
	}

	@DisplayName("Blockchain Test mit assertAll")
	@Tag("schnell")
	@Test
	void testMitAssertAll(TestInfo testInfo) {
		String name = "Blockchain";
		int btc = 30;

		assertAll("Blockchain", //
			() -> assertEquals("Blockchain", name, "Name der Blockchain stimmt nicht überein"), //
			() -> assertEquals("schnell", testInfo.getTags().iterator().next()), //
			() -> assertEquals(30, btc, "Btc stimmt nicht überein"), //
			() -> assertEquals("Blockchain Test mit assertAll", testInfo.getDisplayName())); //
	}

}
